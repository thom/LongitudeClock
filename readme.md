
# LongitudeClock

## Purpose
The Longitude Clock is a visual representation of the procedure of how to determine
the terrestrial longitude from time measurements.

## Background
In navigation, determining latitude via celestial objects is rather simple. However,
deriving longitude is very difficult. This became an increasingly pressing issue
in the 17th and 18th century, when transatlantic and even world wide voyages via
ships were common enterprises. Many fleets were lost and numerous sailors lost
their lives because of the large uncertainty in the procedures that were at hand
to determine longitude. Thanks to John Harrison's ingenuity, a British clockmaker
of the 18th century, the method of determining longitude from precise time
measurements soon became the most reliable tool.

It is based on the rotation of the Earth and the apparent diurnal path of the Sun.
Local noon, i.e. 12:00 Apparent Solar Time or True Solar Time (TST) is defined as
the point in time when the Sun attains its highest elevation above the horizon.
This is either due South (in the northern hemisphere) or North (in the southern
hemisphere). The duration between two consecutive local noons on average lasts
24 hours. This results to an angular rate of 15&deg; longitude per hour.

The solar time is nowadays detached from the civil time, the time the clock shows
us. It is based on a convention that assigns time zones to broad ranges of
longitude, loosely tied to the hourly 15 degree separation.

## The math behind the clock
During the age of exploration, navigational measurements during the day were
typically done at 12:00 local noon TST. An accurate clock that displays the
current time (TST) at the Prime Meridian at 0&deg; longitude permits calculating
the time difference between ones own location and the longitude reference, the
Prime Meridian. Since one hour time difference corresponds to 15&deg;, the time
difference in hours multiplied by 15 yields the longitude in degrees. The sign
of that value corresponds to either western or eastern longitudes.

<center>
&lambda; = &Delta;t &times; 15&deg;/h = (12 h - TST) &times; 15&deg;/h
</center>

## Usage
The Longitude Clock assumes TST for time keeping. It consists of two disks, one
for the northern (left) and another for the southern hemisphere (right). The
inner disks show corresponding maps. The coordinate grid contains longitudes
separated by 15&deg; or one hour. The outer yellow rings are the time indicators
given in TST. The local TST is always assumed to be noon. This is indicated by
a red line that rotates with the face of the clock.

To determine the longitude, enter the time for the Prime Meridian. It is indicated
by a black triangle at the edge of the maps. The time can be set either by dragging
the rings with the mouse or by entering it into the field at the bottom of the
window and pressing ENTER. The longitude is then indicated by the red line
indicating local noon.

## Prerequisites
* Java version 8 or higher
* Graphics board that supports at least OpenGL 3.3.  
  When starting the application, the OpenGL version that is currently supported
  will be shown in a separate console.

## Quick-Ref
* Start the application the application either by double click on
  LongitudeClock.jar or on one of the run scripts: run_linux.sh or run_windows.bat
* Move the ring in order to set the time (TST, True Solar Time or Apparent Solar Time)
* You can also increase/decrease time by pressing '+' or '-' on the normal
  keyboard (not the numpad!)
* Press 'Esc' to leave the application.


## Authors
* Dr. Thomas Müller,  
  Haus der Astronomie, MPIA-Campus,  
  69117 Heidelberg  
  Email: tmueller@mpia.de  
* Idea by
  Dr. Markus Nielbock,  
  Haus der Astronomie, MPIA-Campus,  
  69117 Heidelberg  
  Email: nielbock@hda-hd.de  


## Licence
[CC BY 3.0 Unported](https://creativecommons.org/licenses/by/3.0/legalcode)

## Sources
* [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [jogamp 2.3.2](https://jogamp.org/deployment/archive/rc/v2.3.2/)
* Image textures of the northern and southern hemispheres are by Sean Baker
  [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/legalcode))
    - [Northern Hemisphere](https://commons.wikimedia.org/wiki/File:Northern_Hemisphere_LamAz.png)
    - [Southern Hemisphere](https://commons.wikimedia.org/wiki/File:Southern_Hemisphere_LamAz.png)


## How to compile LongitudeClock yourself
* You have to install the Java 8 JDK as well as the jogamp 2.3.2 library first.
* Assuming JDK is installed in /usr/local/Java/jdk1.8 and jogamp in 
  /usr/local/Java/jogamp, then go to the LongitudeClock folder where build.xml
  can be found.
* On 64bit Linux run:
    ant -f build.xml -DJDK_dir=/usr/local/Java/jdk1.8 -Djogl_dir=/usr/local/Java/jogamp/ jar
    
