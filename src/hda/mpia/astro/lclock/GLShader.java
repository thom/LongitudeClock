package hda.mpia.astro.lclock;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import com.jogamp.opengl.*;

/**
 * GLShader convenience class
 * 
 * @author Thomas Mueller, HdA
 */
public class GLShader {
    protected GL3 gl;
    protected int prog;

    public GLShader(GL3 gl) {
        this.gl = gl;
        this.prog = 0;
    }

    /**
     * Bind shader program.
     */
    public void Bind() {
        gl.glUseProgram(prog);
    }

    /**
     * Create empty shader program.
     */
    public void CreateEmptyProgram() {
        if (prog != 0) {
            RemoveAllShaders();
        }
        prog = gl.glCreateProgram();
    }

    /**
     * Create shader program from file where only vertex and fragment shader are
     * given.
     * 
     * @param vsFilename
     *            Filename of vertex shader
     * @param fsFilename
     *            Filename of fragment shader
     * @return true if shader could be compiled
     */
    public boolean CreateProgramFromFile(String vsFilename, String fsFilename) {
        int vShader = createShaderFromFile(vsFilename, GL3.GL_VERTEX_SHADER);
        if (vShader == 0) return false;

        int fShader = createShaderFromFile(fsFilename, GL3.GL_FRAGMENT_SHADER);
        if (fShader == 0) return false;

        prog = gl.glCreateProgram();
        gl.glAttachShader(prog, vShader);
        gl.glAttachShader(prog, fShader);
        return Link();
    }

    /**
     * Create shader program from file where only vertex, geometry, and fragment
     * shader are given
     * 
     * @param vsFilename
     *            Filename of vertex shader
     * @param gsFilename
     *            Filename of geometry shader
     * @param fsFilename
     *            Filename of fragment shader
     * @return true if shader could be compiled
     */
    public boolean CreateProgramFromFile(String vsFilename, String gsFilename,
            String fsFilename) {
        int vShader = createShaderFromFile(vsFilename, GL3.GL_VERTEX_SHADER);
        if (vShader == 0) return false;
        int gShader = createShaderFromFile(gsFilename, GL3.GL_GEOMETRY_SHADER);
        if (gShader == 0) return false;
        int fShader = createShaderFromFile(fsFilename, GL3.GL_FRAGMENT_SHADER);
        if (fShader == 0) return false;

        prog = gl.glCreateProgram();
        gl.glAttachShader(prog, vShader);
        gl.glAttachShader(prog, gShader);
        gl.glAttachShader(prog, fShader);
        return Link();
    }

    /**
     * Create shader program from strings where only vertex and fragment shader
     * are given.
     * 
     * @param vsText
     *            Vertex shader text
     * @param fsText
     *            Fragment shader text
     * @return true if shader could be compiled
     */
    public boolean CreateProgramFromString(String vsText, String fsText) {
        int vShader = createShaderFromString(vsText, GL3.GL_VERTEX_SHADER);
        if (vShader == 0) return false;

        int fShader = createShaderFromString(fsText, GL3.GL_FRAGMENT_SHADER);
        if (fShader == 0) return false;

        prog = gl.glCreateProgram();
        gl.glAttachShader(prog, vShader);
        gl.glAttachShader(prog, fShader);
        return Link();
    }

    /**
     * Create shader program from strings where only vertex, geometry, and
     * fragment shader are given
     * 
     * @param vsText
     *            Vertex shader text
     * @param gsText
     *            Geometry shader text
     * @param fsText
     *            Fragment shader text
     * @return true if shader could be compiled
     */
    public boolean CreateProgramFromString(String vsText, String gsText,
            String fsText) {
        int vShader = createShaderFromString(vsText, GL3.GL_VERTEX_SHADER);
        if (vShader == 0) return false;
        int gShader = createShaderFromString(gsText, GL3.GL_GEOMETRY_SHADER);
        if (gShader == 0) return false;
        int fShader = createShaderFromString(fsText, GL3.GL_FRAGMENT_SHADER);
        if (fShader == 0) return false;

        prog = gl.glCreateProgram();
        gl.glAttachShader(prog, vShader);
        gl.glAttachShader(prog, gShader);
        gl.glAttachShader(prog, fShader);
        return Link();
    }


    /**
     * Get shader program handle.
     * 
     * @return
     */
    public int GetProgHandle() {
        return this.prog;
    }

    /**
     * Link program
     * 
     * @return true if linking was successful
     */
    public boolean Link() {
        gl.glLinkProgram(prog);
        boolean status = printProgramInfoLog();
        gl.glUseProgram(0);
        return status;
    }

    /**
     * Unbind shader program
     */
    public void Release() {
        gl.glUseProgram(0);
    }

    /**
     * Remove all attached shaders from program and delete the program. This
     * function cannot be called directly.
     */
    public void RemoveAllShaders() {
        if (prog == 0) {
            return;
        }

        if (!gl.glIsProgram(prog)) {
            return;
        }

        int maxCount = 1024;
        int[] count = new int[1];
        int[] shaders = new int[maxCount];
        gl.glUseProgram(0);

        gl.glGetAttachedShaders(prog, maxCount, count, 0, shaders, 0);
        for (int i = 0; i < count[0]; i++) {
            gl.glDetachShader(prog, shaders[i]);
            gl.glDeleteShader(shaders[i]);
        }
        gl.glDeleteProgram(prog);
        prog = 0;
    }

    /**
     * Get location of uniform variable
     * 
     * @param name
     *            Name of uniform variable
     * @return uniform handle
     */
    public int GetUniformLocation(String name) {
        return gl.glGetUniformLocation(prog, name);
    }

    /**
     * Create shader from file. Read shader text from file and compile it as
     * shader of type 'type'.
     * 
     * @param filename
     *            Filename of shader
     * @param type
     *            Type of shader (GL_VERTEX_SHADER, etc.)
     * @return shader handle
     */
    protected int createShaderFromFile(String filename, int type) {
        String[] shaderText = new String[1];
        if (!readShaderFromFile(filename, shaderText)) {
            return 0;
        }

        int shader = gl.glCreateShader(type);
        gl.glShaderSource(shader, 1, shaderText, null);
        gl.glCompileShader(shader);
        // printShaderInfoLog("shader:", shader);
        return shader;
    }

    protected int createShaderFromString(String text, int type) {
        String[] shaderText = new String[1];
        shaderText[0] = text;
        int shader = gl.glCreateShader(type);
        gl.glShaderSource(shader, 1, shaderText, null);
        gl.glCompileShader(shader);
        // printShaderInfoLog("shader:", shader);
        return shader;
    }

    /**
     * Read shader text from file
     * 
     * @param filename
     *            Filename of shader
     * @param shaderText
     *            Shader text
     * @return true if file could be read
     */
    protected boolean readShaderFromFile(String filename, String[] shaderText) {
        Path path = Paths.get(filename);
        shaderText[0] = "";
        try {
            Scanner scanner = new Scanner(path);
            while (scanner.hasNextLine()) {
                shaderText[0] += scanner.nextLine() + "\n";
            }
            scanner.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Print shader info log
     * 
     * @param text
     * @param handle
     * @return
     */
    protected boolean printShaderInfoLog(String text, int handle) {
        int[] status = new int[1];
        int[] infoLogLen = new int[1];

        gl.glGetShaderiv(handle, GL2.GL_COMPILE_STATUS, status, 0);
        gl.glGetShaderiv(handle, GL2.GL_INFO_LOG_LENGTH, infoLogLen, 0);

        if (status[0] == 1 || infoLogLen[0] < 1) {
            return true;
        }

        int[] charsWritten = new int[1];
        byte[] infoLog = new byte[infoLogLen[0]];

        gl.glGetShaderInfoLog(handle, infoLogLen[0], charsWritten, 0, infoLog,
                0);

        // System.err.println("InfoLog");
        // for (int i = 0; i < charsWritten[0]; i++) {
        // System.err.format("%c", infoLog[i]);
        // }
        // System.err.println();
        return false;
    }

    /**
     * Print program info log
     * 
     * @return
     */
    protected boolean printProgramInfoLog() {
        int[] infoLogLen = new int[1];
        int[] linkStatus = new int[1];
        int[] charsWritten = new int[1];

        gl.glGetProgramiv(prog, GL2.GL_INFO_LOG_LENGTH, infoLogLen, 0);
        gl.glGetProgramiv(prog, GL2.GL_LINK_STATUS, linkStatus, 0);

        if (linkStatus[0] == 0 && infoLogLen[0] > 1) {
            byte[] infoLog = new byte[infoLogLen[0]];
            gl.glGetProgramInfoLog(prog, infoLogLen[0], charsWritten, 0,
                    infoLog, 0);
            System.err.println("ProgramInfoLog:");
            for (int i = 0; i < charsWritten[0]; i++) {
                System.err.format("%c", infoLog[i]);
            }
            return false;
        }
        return true;
    }
}
