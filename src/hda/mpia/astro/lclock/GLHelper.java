package hda.mpia.astro.lclock;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

public class GLHelper {
    public static boolean setOrthoMatrix(float l, float r, float b, 
            float t, float n, float f, FloatBuffer matrix) {
        if (matrix.capacity() < 16) {
            return false;
        }
        float[] om = new float[]{2.0f / (r - l), 0.0f, 0.0f, 0.0f, 0.0f,
                2.0f / (t - b), 0.0f, 0.0f, 0.0f, 0.0f, -2.0f / (f - n), 0.0f,
                -(r + l) / (r - l), -(t + b) / (t - b), -(f + n) / (f - n),
                1.0f};
        for (int i = 0; i < 16; i++) {
            matrix.put(i, om[i]);
        }
        return true;
    }
    
    
    public static IntBuffer  readImage(GL3 gl, String filename) {
        IntBuffer buf = IntBuffer.allocate(1);
        buf.put(0, 0);
        try {
            BufferedImage bimg = ImageIO.read(new File(filename));
            
            int width = bimg.getWidth();
            int height = bimg.getHeight();
            int[] pixelIntData = new int[width * height];
            bimg.getRGB(0, 0, width, height, pixelIntData, 0, width);
            ByteBuffer buffer = ByteBuffer.allocateDirect(pixelIntData.length * 4);
            buffer.order(ByteOrder.nativeOrder());
            // Unpack the data, each integer into 4 bytes of the ByteBuffer.
            // Also we need to vertically flip the image because the image origin
            // in OpenGL is the lower-left corner.
            for(int y=0; y < height; y++) {
              int k = (height-1-y) * width;
              for(int x=0; x < width; x++) {
                  buffer.put((byte)(pixelIntData[k]>>> 16));
                  buffer.put((byte)(pixelIntData[k]>>> 8));
                  buffer.put((byte)(pixelIntData[k]));
                  buffer.put((byte)(pixelIntData[k]>>> 24));
                  k++;
              }
            }
            buffer.rewind();
            
            gl.glGenTextures(1, buf);
            gl.glBindTexture(GL.GL_TEXTURE_2D, buf.get(0));
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);            
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);            
            gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, bimg.getWidth(), bimg.getHeight(), 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, buffer);
            gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return buf;
    }
}
