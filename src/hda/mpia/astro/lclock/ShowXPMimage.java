package hda.mpia.astro.lclock;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

public class ShowXPMimage {

    private int width;
    private int height;
    private int numColors;
    private int colorCodeSize;
    private Map<String, Color> colorMap;
    private Map<Character, Integer> hexMap;
    private ByteBuffer imgData = null;
    
    private IntBuffer texID = IntBuffer.allocate(1);
    private GLShader shader;
    private VertexArray va;
    private FloatBuffer projMX = FloatBuffer.allocate(16);
    
    public ShowXPMimage(String[] xpm) {
        colorMap = new HashMap<String, Color>();
        hexMap = new HashMap<Character, Integer>();
        hexMap.put('0', 0);
        hexMap.put('1', 1);
        hexMap.put('2', 2);
        hexMap.put('3', 3);
        hexMap.put('4', 4);
        hexMap.put('5', 5);
        hexMap.put('6', 6);
        hexMap.put('7', 7);
        hexMap.put('8', 8);
        hexMap.put('9', 9);
        hexMap.put('A', 10);
        hexMap.put('B', 11);
        hexMap.put('C', 12);
        hexMap.put('D', 13);
        hexMap.put('E', 14);
        hexMap.put('F', 15);

        width = height = numColors = colorCodeSize = 0;
        SetImage(xpm);
    }
    
    public void Draw(GL3 gl) {
        shader.Bind();
        gl.glUniformMatrix4fv(shader.GetUniformLocation("projMX"), 1, false, projMX);
        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, texID.get(0));
        va.Bind();
        gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, 4);
        va.Release();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
        shader.Release();
    }
    
    public void Init(GL3 gl) {
        if (width == 0 || height == 0 || imgData == null) {
            return;
        }
        gl.glGenTextures(1, texID);
        gl.glBindTexture(GL.GL_TEXTURE_2D, texID.get(0));
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, width, height, 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, imgData);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
        
        shader = new GLShader(gl);
        shader.CreateProgramFromString(vShaderText, fShaderText);
        
        float[] verts = {0.0f,0.0f, 1.0f,0.0f, 0.0f,1.0f, 1.0f,1.0f};
        FloatBuffer data = Buffers.newDirectFloatBuffer(verts); 
        va = new VertexArray(gl);
        va.Create(4);
        va.SetArrayBuffer(0, GL.GL_FLOAT, 2, data);
        
        GLHelper.setOrthoMatrix(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, projMX);
    }
    
    public Color GetColor(String code) {
        if (!colorMap.containsKey(code)) {
            return Color.black;
        }
        return colorMap.get(code);
    }
    
    public int GetHeight() {
        return height;
    }

    public int GetNumColors() {
        return numColors;
    }

    public int GetTexID() {
        return texID.get(0);
    }
    
    public int GetWidth() {
        return width;
    }
    
    public void ReloadShaders() {
        shader.RemoveAllShaders();
        shader.CreateProgramFromString(vShaderText, fShaderText);
    }
    
    public boolean SetImage(String[] xpm) {
        if (xpm.length == 0) {
            return false;
        }
        String header = xpm[0];
        String[] h = header.split(" ");
        if (h.length < 4) {
            return false;
        }
        
        width = Integer.parseInt(h[0]);
        height = Integer.parseInt(h[1]);
        numColors = Integer.parseInt(h[2]);
        colorCodeSize = Integer.parseInt(h[3]);
      
        if (xpm.length < numColors + 1 || colorCodeSize < 1) {
            return false;
        }
        
        for(int i=1; i <= numColors; i++) {
            String code = xpm[i].substring(0, colorCodeSize);
            String cstr = xpm[i].substring(5, xpm[i].length());
            if (cstr.compareTo("None")==0) {
                colorMap.put(code, Color.black);
            }
            else {
                //System.out.println(code + " " + cstr);
                int r = parseHex(cstr.substring(1,3));
                int g = parseHex(cstr.substring(3,5));
                int b = parseHex(cstr.substring(5,7));
                Color color = new Color(r, g, b);
                colorMap.put(code, color);
            }
            //System.out.println(code + " " + color);
        }
        
        imgData = ByteBuffer.allocate((width+1) * (height+1) * 3);
        int count = 0;
        for(int row = 0; row < height; row++) {
           String line = xpm[row + 1 + numColors];
           //System.out.println(row + " " + line.length());
           for(int n=0; n < width; n++) {
               Color col = colorMap.get(line.substring(n*colorCodeSize, (n+1)*colorCodeSize));
               imgData.put(3*count+0, (byte)col.getRed());
               imgData.put(3*count+1, (byte)col.getGreen());
               imgData.put(3*count+2, (byte)col.getBlue());
               count += 1;
           }
        }
        //System.out.println(width + " " + height + "    " + count + " " + width*height);
        return true;
    }


    
    private int parseHex(String hs) {
        int a = hexMap.get(hs.charAt(0));
        int b = hexMap.get(hs.charAt(1));
        return (a<<4) + b;
    }
    
    private String vShaderText = 
            "#version 330\n" + 
            "uniform mat4 projMX;\n;" +
            "layout(location = 0) in vec2 pos;\n" +
            "out vec2 texCoords;\n" +
            "void main() {\n" +
            "    gl_Position = projMX * vec4(pos,0,1);\n" +
            "    texCoords = vec2(pos.x,1-pos.y);\n" +
            "}\n";
        
    private String fShaderText = 
            "#version 330\n" + 
            "uniform sampler2D tex;\n" +
            "in vec2 texCoords;\n" +
            "layout(location = 0) out vec4 fragColor;\n" +
            "void main() {\n" + 
            "    fragColor = texture(tex, texCoords);\n" +
            "}\n";
}
