package hda.mpia.astro.lclock;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.event.HyperlinkEvent;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import hda.mpia.astro.lclock.MyMouseAdapter;


@SuppressWarnings("serial")
/**
 * LongitudeClock ...
 * 
 * @author Thomas Mueller, HdA
 *
 */
public class JLongitudeClock extends JFrame implements KeyListener {

    private GLCanvas glCanvas;
    private Renderer renderer;
    private MyMouseAdapter mouseAdapter = null;
    
    private JLabel labClock = null;
    private JFormattedTextField jftClock = null;
    private JButton btnCurrentTime = null;
//    private JButton btnStepForward = null;
//    private JButton btnStepBackward = null;
    
    private JMenuBar jMenuBar;
    private JMenu jMenuFile;
    private JMenuItem jMenuItemQuit;
    private JMenuItem jMenuItemAbout;
    private JMenuItem jMenuItemHelp;
    
    static int width  = 1280;
    static int height = 720;

    private Font font = new Font("Times", Font.BOLD, 14);
    
    private double PHI_MINUTE_STEP = 360.0 / (24.0 * 60.0);
    
    private DateFormat dateFormat = new SimpleDateFormat("HH:mm");
    private Calendar calendar;
    
    private JEditorPane HelpText = null;
    
    /**
     * LongitudeClock 
     */
    public JLongitudeClock() {
        setSize(width, height);
        setPreferredSize(new java.awt.Dimension(width, height));
        setResizable(false);
        
        // to prevent glcanvas drawing over the jmenubar
        JPopupMenu.setDefaultLightWeightPopupEnabled(false); 
        
        Locale englishLoc = new Locale("en_US");
        Locale.setDefault(englishLoc);
        
        GLProfile glp = GLProfile.getGL2GL3();
        GLCapabilities caps = new GLCapabilities(glp);
        caps.setDoubleBuffered(true);

        glCanvas = new GLCanvas(caps);
        glCanvas.setMinimumSize(new java.awt.Dimension(400, 800));
        glCanvas.setMaximumSize(new java.awt.Dimension(400, 800));

        renderer = new Renderer(JLongitudeClock.width, JLongitudeClock.height);
        glCanvas.addKeyListener(this);
        glCanvas.addGLEventListener(renderer);
        glCanvas.requestFocusInWindow();

        mouseAdapter = new MyMouseAdapter(glCanvas, this);
        glCanvas.addMouseListener(mouseAdapter);
        glCanvas.addMouseMotionListener(mouseAdapter);

        initMenu();
        initGUI();
        
        setTitle("LongitudeClock");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        dateFormat.setCalendar(calendar);
        
        setCurrentDate();
        initHelpText();        
    }
    
    /**
     * 
     */
    public void close() {
        super.dispose();
        System.out.println("Bye bye");
        System.exit(0);
    }

    /**
     * 
     */
    public void dispose() {
        close();
    }
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JLongitudeClock().setVisible(true);
            }
        });
    }

    
    /**
     * keypressed event callback
     * @param e
     */
    @Override    
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE: {
                close();
            }
            case KeyEvent.VK_S: {
                renderer.ReloadShaders();
                glCanvas.repaint();
                break;
            }
            case KeyEvent.VK_R: {
                Reset();
                break;
            }
            case KeyEvent.VK_PLUS: {
                Rotate(Math.toRadians(PHI_MINUTE_STEP));
                break;
            }
            case KeyEvent.VK_MINUS: {
                Rotate(-Math.toRadians(PHI_MINUTE_STEP));
                break;
            }
        }
    }

    /**
     * key released callback 
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        // nothing to do
    }

    
    /**
     * key typed callback
     * @param e
     */
    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do
    }
    
    
    /**
     * Convert angle phi to 24-hour time
     * @param phi
     * @param hm
     */
    public void phiToTime(double phi, int[] hm) {
        double h = fmod(Math.PI - phi, 2.0 * Math.PI) / (2.0 * Math.PI) * 24.0;
        hm[0] = (int)(h);
        hm[1] = (int)(Math.round((h - hm[0]) * 60.0 * 10.0)*0.1);
        if (hm[1] == 60) {
            hm[0] += 1;
            hm[1] = 0;
        }
    }
    
    /**
     * 
     * @param dphi
     */
    public void Rotate(double dphi) { 
        renderer.Rotate(dphi);
        int[] hm = {0,0};
        phiToTime(-renderer.GetAngle(), hm);
        
        jftClock.setText(String.format("%02d:%02d", hm[0], hm[1]));
        glCanvas.repaint();
    }
    
    
    /**
     * 
     */
    public void Reset() {
        renderer.Reset();
        glCanvas.repaint();
    }
    

    /**
     * Initialize GUI
     */
    private void initGUI() {
        Container root = getContentPane();
        root.setLayout(new BorderLayout());
        
        //labClock = new JLabel("Time (GMT): ");
        labClock = new JLabel("Time (TST): ");
        labClock.setFont(font);
        
        jftClock = new JFormattedTextField(dateFormat);
        jftClock.setColumns(10);
        jftClock.setHorizontalAlignment(JLabel.CENTER);
        jftClock.setFont(font);

        jftClock.addActionListener(l -> {
            String text = jftClock.getText();
            try {
                calendar.setTime(dateFormat.parse(text));
                double phi = Math.toRadians((calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) / 60.0) / 24.0 * 360.0);
                renderer.SetAngle(phi + Math.PI);
                glCanvas.repaint();
                jftClock.setText(dateFormat.format(calendar.getTime()));
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            
        });
        
        btnCurrentTime = new JButton("Set Current Time");
        btnCurrentTime.setFont(font);
        btnCurrentTime.addActionListener(l -> {
            setCurrentDate();
        });
        
//        btnStepForward = new JButton(">");
//        btnStepForward.setPreferredSize(new Dimension(52,20));
//        btnStepForward.setFont(font);        
//        btnStepForward.addActionListener(l -> {
//            Rotate(Math.toRadians(PHI_MINUTE_STEP));
//        });
//        
//        btnStepBackward = new JButton("<");
//        btnStepBackward .setPreferredSize(new Dimension(52,20));
//        btnStepBackward .setFont(font);
//        btnStepBackward .addActionListener(l -> {
//            Rotate(Math.toRadians(-PHI_MINUTE_STEP));
//        });
//        
        JPanel panel = new JPanel();
        panel.add(labClock);
        panel.add(jftClock);
        //panel.add(btnCurrentTime);
//        panel.add(btnStepBackward);        
//        panel.add(btnStepForward);
        
        add(panel, BorderLayout.SOUTH);
        add(glCanvas);        
    }
    
    /**
     * 
     */
    void initMenu() {
        jMenuBar = new JMenuBar();
        jMenuFile = new JMenu();

        jMenuItemAbout = new JMenuItem();
        jMenuItemHelp = new JMenuItem();
        jMenuItemQuit = new JMenuItem();
        
        jMenuItemAbout.setText("About");
        jMenuItemAbout.setAccelerator(KeyStroke.getKeyStroke("control A"));
        jMenuItemAbout.addActionListener(e -> {
            JOptionPane.showMessageDialog(this,
                    "LongitudeClock, Version 1.0\n\n" + "Copyright (c) 2016, Thomas Mueller, Markus Nielbock\n"
                            + "Haus der Astronomie (MPIA)\n" + "69117 Heidelberg, Germany\n\n"
                            + "Email: tmueller@mpia.de, nielbock@hda-hd.de",
                    "About", JOptionPane.PLAIN_MESSAGE);
        });
        
        
        jMenuItemHelp.setText("Help");
        jMenuItemHelp.setAccelerator(KeyStroke.getKeyStroke("control H"));
                
        jMenuItemHelp.addActionListener(e -> {
            JOptionPane.showMessageDialog(this,
                    new JScrollPane(HelpText),
                    "Help", JOptionPane.PLAIN_MESSAGE);
        });
        

        jMenuItemQuit.setText("Quit");
        jMenuItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0));
        jMenuItemQuit.addActionListener(e -> {
            close();
        });
        
        jMenuFile.setText("Menu");
        jMenuFile.add(jMenuItemAbout);
        jMenuFile.add(jMenuItemHelp);
        jMenuFile.setMnemonic(KeyEvent.VK_M);
        jMenuFile.add(jMenuItemQuit);

        jMenuBar.add(jMenuFile);
        setJMenuBar(jMenuBar);
    }

    
    private void initHelpText() {
        HelpText = new JEditorPane();
        HelpText.setEditable(false);
        HelpText.setContentType("text/html");
        HelpText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
        HelpText.setPreferredSize(new Dimension(800,500));
        HelpText.setText(getHelpText());
        
        HelpText.addHyperlinkListener(l -> {
            if (l.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                if(Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(l.getURL().toURI());
                    }
                    catch (IOException | URISyntaxException e) {
                        JOptionPane.showMessageDialog(this,
                                String.format("Cannot open external browser to follow the link:\n\n%s",l.getURL().toString()),
                                "URL error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
    }
    
    /**
     * Set current time
     */
    private void setCurrentDate() {
        calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        //System.out.println(calendar);
        double phi = Math.toRadians((calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) / 60.0) / 24.0 * 360.0);
        jftClock.setText(dateFormat.format(calendar.getTime()));
        renderer.SetAngle(phi + Math.PI);
        glCanvas.repaint();
    }
    
    
    /**
     * 
     * @param x
     * @param m
     * @return
     */
    double fmod(double x, double m) {
        return x - Math.floor(x / m) * m;
    }
    
    String getHelpText() {
        return "<html>"
            + "<head>"
            + "<style type='text/css'>"
            + "       code{white-space: pre;}"
            + "       body{ font: 12px Arial, sans-serif; }"
            + "       p { padding: 1em; margin-left: 10pt }"
            + "       h2 { margin-top: 20pt; margin-bottom:0pt }"
            + "       li { margin-left: 10pt; padding:0.5em}"
            + "</style>"
            + "</head>"
            + "<body>"
            + "<h1 id='longitudeclock'>LongitudeClock</h1>"
            + "<h2 id='purpose'>Purpose</h2>"
            + "<p>The Longitude Clock is a visual representation of the procedure of how to determine the terrestrial longitude from time measurements.</p>"
            + "<h2 id='background'>Background</h2>"
            + "<p>In navigation, determining latitude via celestial objects is rather simple. However, deriving longitude is very difficult. This became an increasingly pressing issue in the 17th and 18th century, when transatlantic and even world wide voyages via ships were common enterprises. Many fleets were lost and numerous sailors lost their lives because of the large uncertainty in the procedures that were at hand to determine longitude. Thanks to John Harrison's ingenuity, a British clockmaker of the 18th century, the method of determining longitude from precise time measurements soon became the most reliable tool.</p>"
            + "<p>It is based on the rotation of the Earth and the apparent diurnal path of the Sun. Local noon, i.e. 12:00 Apparent Solar Time or True Solar Time (TST) is defined as the point in time when the Sun attains its highest elevation above the horizon. This is either due South (in the northern hemisphere) or North (in the southern hemisphere). The duration between two consecutive local noons on average lasts 24 hours. This results to an angular rate of 15° longitude per hour.</p>"
            + "<p>The solar time is nowadays detached from the civil time, the time the clock shows us. It is based on a convention that assigns time zones to broad ranges of longitude, loosely tied to the hourly 15 degree separation.</p>"
            + "<h2 id='the-math-behind-the-clock'>The math behind the clock</h2>"
            + "<p>During the age of exploration, navigational measurements during the day were typically done at 12:00 local noon TST. An accurate clock that displays the current time (TST) at the Prime Meridian at 0° longitude permits calculating the time difference between ones own location and the longitude reference, the Prime Meridian. Since one hour time difference corresponds to 15°, the time difference in hours multiplied by 15 yields the longitude in degrees. The sign of that value corresponds to either western or eastern longitudes.</p>"
            + "<center>"
            + "λ = Δt × 15°/h = (12 h - TST) × 15°/h"
            + "</center>"
            + "<h2 id='usage'>Usage</h2>"
            + "<p>The Longitude Clock assumes TST for time keeping. It consists of two disks, one for the northern (left) and another for the southern hemisphere (right). The inner disks show corresponding maps. The coordinate grid contains longitudes separated by 15° or one hour. The outer yellow rings are the time indicators given in TST. The local TST is always assumed to be noon. This is indicated by a red line that rotates with the face of the clock.</p>"
            + "<p>To determine the longitude, enter the time for the Prime Meridian. It is indicated by a black triangle at the edge of the maps. The time can be set either by dragging the rings with the mouse or by entering it into the field at the bottom of the window and pressing ENTER. The longitude is then indicated by the red line indicating local noon.</p>"
            + "<h2 id='prerequisites'>Prerequisites</h2>"
            + "<ul>"
            + "<li>Java version 8 or higher</li>"
            + "<li>Graphics board that supports at least OpenGL 3.3.<br /> When starting the application, the OpenGL version that is currently supported will be shown in a separate console.</li>"
            + "</ul>"
            + "<h2 id='quick-ref'>Quick-Ref</h2>"
            + "<ul>"
            + "<li>Start the application either by double click on LongitudeClock.jar or on one of the run scripts: run_linux.sh or run_windows.bat</li>"
            + "<li>Move the ring in order to set the time (TST, True Solar Time or Apparent Solar Time)</li>"
            + "<li>You can also increase/decrease time by pressing '+' or '-' on the normal keyboard (not the numpad!)</li>"
            + "<li>Press 'Esc' to leave the application.</li>"
            + "</ul>"
            + "<h2 id='authors'>Authors</h2>"
            + "<ul>"
            + "<li>Dr. Thomas Müller,<br /> Haus der Astronomie, MPIA-Campus,<br /> 69117 Heidelberg<br /> Email: tmueller@mpia.de<br /></li>"
            + "<li>Idea by Dr. Markus Nielbock,<br /> Haus der Astronomie, MPIA-Campus,<br /> 69117 Heidelberg<br /> Email: nielbock@hda-hd.de</li>"
            + "</ul>"
            + "<h2 id='licence'>Licence</h2>"
            + "<p><a href='https://creativecommons.org/licenses/by/3.0/legalcode'>CC BY 3.0 Unported</a></p>"
            + "<h2 id='sources'>Sources</h2>"
            + "<ul>"
            + "<li><a href='http://www.oracle.com/technetwork/java/javase/downloads/index.html'>Java 8 JDK</a></li>"
            + "<li><a href='https://jogamp.org/'>jogamp 2.3.2</a></li>"
            + "<li>Image textures of the northern and southern hemispheres are by Sean Baker <a href='https://creativecommons.org/licenses/by/2.0/legalcode'>CC-BY-2.0</a>)"
            + "<ul>"
            + "<li><a href='https://commons.wikimedia.org/wiki/File:Northern_Hemisphere_LamAz.png'>Northern Hemisphere</a></li>"
            + "<li><a href='https://commons.wikimedia.org/wiki/File:Southern_Hemisphere_LamAz.png'>Southern Hemisphere</a></li>"
            + "</ul></li>"
            + "</ul>"
            + "</body>"
            + "</html>";
            };
}
