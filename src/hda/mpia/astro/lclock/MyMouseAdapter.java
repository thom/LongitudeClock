package hda.mpia.astro.lclock;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import com.jogamp.opengl.awt.GLCanvas;

/**
 * Mouse handling
 * 
 * @author Thomas Mueller, HdA
 */
public class MyMouseAdapter extends MouseAdapter
        implements
            MouseMotionListener {

    private GLCanvas canvas = null;
    private JLongitudeClock main = null;

    private int button = MouseEvent.NOBUTTON;
    private int[] mouseLastPos = {0, 0};

    public MyMouseAdapter(GLCanvas canvas, JLongitudeClock main) {
        this.canvas = canvas;
        this.main = main;
    }
    
    private double getAngle(int x, int y) {
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        double cx1 = width*0.25;
        double cy1 = height*0.5;
        double dx1 = x - cx1;
        double dy1 = y - cy1;
        double r1 = Math.sqrt(dx1*dx1 + dy1*dy1);
        
        double cx2 = width*0.75;
        double cy2 = height*0.5;
        double dx2 = x - cx2;
        double dy2 = y - cy2;
        double r2 = Math.sqrt(dx2*dx2 + dy2*dy2);
        
        double phi = 0.0f;
        if (r1 < r2) {
            phi = Math.atan2(dy1, dx1);
        }
        else {
            phi = Math.PI-Math.atan2(dy2, dx2);
        }
        return phi;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
        if (button == MouseEvent.BUTTON1) {
            double phiNew = getAngle(e.getX(), e.getY());
            double phiOld = getAngle(mouseLastPos[0], mouseLastPos[1]);
            main.Rotate(phiNew - phiOld);
        }

        mouseLastPos[0] = e.getX();
        mouseLastPos[1] = e.getY();
        super.mouseDragged(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        button = e.getButton();
        mouseLastPos[0] = e.getX();
        mouseLastPos[1] = e.getY();
        super.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        button = MouseEvent.NOBUTTON;
        super.mouseReleased(e);
    }
}
