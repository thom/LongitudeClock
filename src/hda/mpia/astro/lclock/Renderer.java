package hda.mpia.astro.lclock;

import java.awt.Color;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.DebugGL3;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.util.texture.Texture;


/**
 * Render engine
 * 
 * @author Thomas Mueller, HdA
 */
public class Renderer implements GLEventListener {

    enum Hemisphere {
        North,
        South
    };
    
    //private int ringSize = 30;
    
    private int width;
    private int height;

    private FloatBuffer orthoViewHem;
    private FloatBuffer orthoViewRing;

    private boolean scheduleShaderReload = false;

    private String vChartShaderName = "shader/chart.vert";
    private String fChartShaderName = "shader/chart.frag";
    private GLShader chartShader = null;
    
    private String vRingShaderName = "shader/ring.vert";
    private String fRingShaderName = "shader/ring.frag";
    private GLShader ringShader = null;

    private String vMeridShaderName = "shader/meridian.vert";
    private String fMeridShaderName = "shader/meridian.frag";
    private GLShader meridShader = null;
    
    private VertexArray va = null;
    
    private String northTexFilename = "resources/Northern_Hemisphere_LamAz.png";
    private String southTexFilename = "resources/Southern_Hemisphere_LamAz.png";
    private String ringNTexFilename = "resources/ringN.png";
    private String ringSTexFilename = "resources/ringS.png";

    private IntBuffer texNorthID;
    private IntBuffer texSouthID;
    private IntBuffer texRingNID;
    private IntBuffer texRingSID;
    
    private double rotAngle = 0.0;
    
    private Color hdaColor = new Color(27, 58, 120);

    private float meridianSize = 0.03f;
    private VertexArray vaNullMeridian = null;
    private VertexArray vaLine = null;
    private GLShader lineShader = null;
    
    ShowXPMimage xpmImg;
    
    /**
     * Renderer
     * 
     * @param width
     * @param height
     */
    Renderer(int width, int height) {
        this.width = width;
        this.height = height;

        String basePath = System.getProperty("user.dir");
        vChartShaderName = basePath + "/" + vChartShaderName;
        fChartShaderName = basePath + "/" + fChartShaderName;
        
        vRingShaderName = basePath + "/" + vRingShaderName;
        fRingShaderName = basePath + "/" + fRingShaderName;
        
        vMeridShaderName = basePath + "/" + vMeridShaderName;
        fMeridShaderName = basePath + "/" + fMeridShaderName;
        
        northTexFilename = basePath + "/" + northTexFilename;
        southTexFilename = basePath + "/" + southTexFilename;
        ringNTexFilename = basePath + "/" + ringNTexFilename;        
        ringSTexFilename = basePath + "/" + ringSTexFilename;
    }

    public void Rotate(double dphi) {
        rotAngle += dphi;
    }
    
    public void Reset() {
        rotAngle = 0;
    }
    
    public void SetAngle(double phi) {
        rotAngle = phi;
    }
    
    public double GetAngle() {
        return rotAngle;
    }
    
    /**
     * Schedule shader reloading
     */
    public void ReloadShaders() {
        if (chartShader != null) {
            scheduleShaderReload = true;
        }
    }

    /**
     * Initialize Renderer - Check and show the graphics board parameters. 
     * 
     * @param drawable
     */
    @Override
    public void init(GLAutoDrawable drawable) {
        drawable.setGL(new DebugGL3(drawable.getGL().getGL3()));
        GL3 gl = drawable.getGL().getGL3();
        gl.glClearColor(hdaColor.getRed()/255.0f, hdaColor.getGreen()/255.0f,
                hdaColor.getBlue()/255.0f, 0.0f);

        System.out.println("Graphics board details:");
        System.out.printf("\tVendor         : %s\n",
                gl.glGetString(GL3.GL_VENDOR));
        System.out.printf("\tGPU            : %s\n",
                gl.glGetString(GL3.GL_RENDERER));
        System.out.printf("\tOpenGL version : %s\n",
                gl.glGetString(GL3.GL_VERSION));
        System.out.printf("\tGLSL version   : %s\n",
                gl.glGetString(GL2.GL_SHADING_LANGUAGE_VERSION));

        chartShader = new GLShader(gl);
        ringShader = new GLShader(gl);
        meridShader = new GLShader(gl);
        ReloadShaders();

        orthoViewHem = Buffers.newDirectFloatBuffer(16);
        orthoViewRing = Buffers.newDirectFloatBuffer(16);

        float[] qpoints = {-1.0f,-1.0f, 1.0f,-1.0f, -1.0f,1.0f, 1.0f,1.0f};
        FloatBuffer quad = Buffers.newDirectFloatBuffer(qpoints);
        
        va = new VertexArray(gl);
        va.Create(4);
        va.SetArrayBuffer(0, GL.GL_FLOAT, 2, quad, GL.GL_STATIC_DRAW);

        texNorthID = GLHelper.readImage(gl, northTexFilename);
        texSouthID = GLHelper.readImage(gl, southTexFilename);
        texRingNID = GLHelper.readImage(gl, ringNTexFilename);
        texRingSID = GLHelper.readImage(gl, ringSTexFilename);

        float ts = meridianSize;
        float[] tpoints = {-ts,ts, ts,ts, 0,-ts};
        FloatBuffer triangle = Buffers.newDirectFloatBuffer(tpoints);
        
        vaNullMeridian = new VertexArray(gl);
        vaNullMeridian.Create(3);
        vaNullMeridian.SetArrayBuffer(0, GL.GL_FLOAT, 2, triangle);
        
        float[] lpoints = {0.0f,0.0f, 1.0f,0.0f};
        FloatBuffer line = Buffers.newDirectFloatBuffer(lpoints);
        vaLine = new VertexArray(gl);
        vaLine.Create(2);
        vaLine.SetArrayBuffer(0,  GL.GL_FLOAT, 2, line);
        lineShader = new GLShader(gl);
        
        gl.glDisable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        
        xpmImg = new ShowXPMimage(HDALogo.hda_logo_2_xpm);
        xpmImg.Init(gl);
    }

    /**
     * Dispose
     * 
     * @param drawable
     */
    @Override
    public void dispose(GLAutoDrawable drawable) {
        chartShader.RemoveAllShaders();
        ringShader.RemoveAllShaders();
        meridShader.RemoveAllShaders();
        lineShader.RemoveAllShaders();
    }

    /**
     * Render method
     * 
     * @param drawable
     */
    @Override
    public void display(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);

        if (scheduleShaderReload) {
            //System.out.println("Reload shaders");
            chartShader.RemoveAllShaders();
            ringShader.RemoveAllShaders();
            meridShader.RemoveAllShaders();
            lineShader.RemoveAllShaders();
            
            /*
            chartShader.CreateProgramFromFile(vChartShaderName, fChartShaderName);
            ringShader.CreateProgramFromFile(vRingShaderName, fRingShaderName);
            meridShader.CreateProgramFromFile(vMeridShaderName, fMeridShaderName);
            */
            
            chartShader.CreateProgramFromString(Shaders.vChartShaderText, Shaders.fChartShaderText);
            meridShader.CreateProgramFromString(Shaders.vMeridianShaderText, Shaders.fMeridianShaderText);
            ringShader.CreateProgramFromString(Shaders.vRingShaderText, Shaders.fRingShaderText);
            lineShader.CreateProgramFromString(Shaders.vLineShaderText, Shaders.fLineShaderText);
            scheduleShaderReload = false;
        }
        
        int iw = 140;
        gl.glViewport(this.width - iw, 0, iw, iw/2);
        xpmImg.Draw(gl);
        
        drawHemisphere(gl, Hemisphere.North);
        drawHemisphere(gl, Hemisphere.South);
        
        drawMeridian(gl, Hemisphere.North);
        drawMeridian(gl, Hemisphere.South);
        
        gl.glLineWidth(2);
        drawLine(gl, Hemisphere.North);
        drawLine(gl, Hemisphere.South);
        gl.glLineWidth(1);
        
        drawRing(gl, Hemisphere.North);
        drawRing(gl, Hemisphere.South);

    }
    
    /**
     * Draw hemispheres
     * @param gl
     * @param hem
     */
    void drawHemisphere(GL3 gl, Hemisphere hem) {
        if (hem == Hemisphere.North) {
            gl.glViewport(0, 0, width/2, height);
        } else {
            gl.glViewport(width/2, 0, width/2, height);
        }
        
        chartShader.Bind();
        gl.glUniformMatrix4fv(chartShader.GetUniformLocation("projMX"), 1, false, orthoViewHem);
        
        gl.glActiveTexture(GL.GL_TEXTURE0);
        if (hem == Hemisphere.North) {
            gl.glBindTexture(GL.GL_TEXTURE_2D, texNorthID.get(0));
        }
        else {
            gl.glBindTexture(GL.GL_TEXTURE_2D, texSouthID.get(0));
        }
        gl.glUniform1i(chartShader.GetUniformLocation("tex"), 0);
        va.Bind();
        gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, 4);
        va.Release();
        chartShader.Release();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }
    
    /**
     * Draw lines
     * @param gl
     * @param hem
     */
    void drawLine(GL3 gl, Hemisphere hem) {
        double rot = rotAngle + Math.PI*0.5;
        if (hem == Hemisphere.North) {
            gl.glViewport(0, 0, width/2, height);
        } else {
            gl.glViewport(width/2, 0, width/2, height);
            rot = -rotAngle - Math.PI*0.5;
        }
        
        lineShader.Bind();
        gl.glUniformMatrix4fv(lineShader.GetUniformLocation("projMX"), 1, false, orthoViewHem);
        gl.glUniform1f(lineShader.GetUniformLocation("rot"), (float)(rot));
        vaLine.Bind();
        gl.glDrawArrays(GL.GL_LINES, 0, 2);
        vaLine.Release();
        lineShader.Release();
    }
    
    
    /**
     * Draw rings
     * @param gl
     * @param hem
     */
    void drawRing(GL3 gl, Hemisphere hem) {
        double rot = rotAngle;
        if (hem == Hemisphere.North) {
            gl.glViewport(0, 0, width/2, height);
        } else {
            rot = -rotAngle;
            gl.glViewport(width/2, 0, width/2, height);
        }
        
        ringShader.Bind();
        gl.glUniformMatrix4fv(ringShader.GetUniformLocation("projMX"), 1, false, orthoViewRing);

        gl.glActiveTexture(GL.GL_TEXTURE0);
        if (hem == Hemisphere.North) {
            gl.glBindTexture(GL.GL_TEXTURE_2D, texRingNID.get(0));
        } else {
            gl.glBindTexture(GL.GL_TEXTURE_2D, texRingSID.get(0));
        }
        
        gl.glUniform1i(ringShader.GetUniformLocation("tex"), 0);
        
        gl.glUniform1f(ringShader.GetUniformLocation("rot"), (float)rot);
        gl.glUniform1i(ringShader.GetUniformLocation("hemisphere"), (hem == Hemisphere.North ? 0 : 1));
        va.Bind();
        gl.glDrawArrays(GL.GL_TRIANGLE_STRIP, 0, 4);
        va.Release();
        
        ringShader.Release();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }

    
    /**
     * Draw meridian indication triangles
     * @param gl
     * @param hem
     */
    void drawMeridian(GL3 gl, Hemisphere hem) {        
        if (hem == Hemisphere.North) {
            gl.glViewport(0, 0, width/2, height);
        } else {
            gl.glViewport(width/2, 0, width/2, height);
        }
        
        meridShader.Bind();
        gl.glUniformMatrix4fv(meridShader.GetUniformLocation("projMX"), 1, false, orthoViewHem);
        gl.glUniform1i(meridShader.GetUniformLocation("hemisphere"), (hem == Hemisphere.North ? 0 : 1));
        gl.glUniform1f(meridShader.GetUniformLocation("meridSize"), meridianSize);
        
        vaNullMeridian.Bind();
        gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3);
        vaNullMeridian.Release();
        meridShader.Release();
    }
    
    /**
     * Reshape windows
     * 
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height
     */
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
            int height) {
        this.width = width;
        this.height = height;
        
        float aspect = this.width*0.5f / (float) this.height;
        float s = 1.02f;
        GLHelper.setOrthoMatrix(-s*aspect, s*aspect, -s, s, -1.0f, 1.0f, orthoViewRing);
        s = 1.05f;
        GLHelper.setOrthoMatrix(-s*aspect, s*aspect, -s, s, -1.0f, 1.0f, orthoViewHem);
    }

}
