package hda.mpia.astro.lclock;

import java.nio.Buffer;
import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;

/**
 * Vertex array object convenience class
 * 
 * @author Thomas Mueller, HdA
 */
public class VertexArray {
    private GL3 gl;
    private int[] va;
    private int numVertices;
    private int maxVertexAttribs;

    private int[] vbo;
    private int[] isBOSet;
    private int[] vboType;
    private int[] vboUsage;
    private int[] sizeOfData;
    private int[] dim;

    /**
     * Standard constructor
     * 
     * @param gl
     */
    public VertexArray(GL3 gl) {
        this.gl = gl;
        numVertices = 0;
        this.va = new int[1];
        this.va[0] = 0;
    }

    /**
     * Bind vertex array
     */
    public void Bind() {
        gl.glBindVertexArray(va[0]);
    }

    /**
     * Create vertex array
     * 
     * @param numVerts
     *            Number of vertices to be stored in the VA.
     */
    public void Create(int numVerts) {
        numVertices = numVerts;
        IntBuffer maxAttribs = IntBuffer.allocate(1);
        gl.glGetIntegerv(GL3.GL_MAX_VERTEX_ATTRIBS, maxAttribs);
        maxVertexAttribs = maxAttribs.get(0);

        vbo = new int[maxVertexAttribs];
        isBOSet = new int[maxVertexAttribs];
        vboType = new int[maxVertexAttribs];
        vboUsage = new int[maxVertexAttribs];
        sizeOfData = new int[maxVertexAttribs];
        dim = new int[maxVertexAttribs];

        for (int i = 0; i < maxVertexAttribs; i++) {
            vbo[i] = 0;
            isBOSet[i] = 0;
            vboType[i] = GL3.GL_FLOAT;
            vboUsage[i] = GL3.GL_STATIC_DRAW;
            sizeOfData[i] = Float.BYTES;
            dim[i] = 1;
        }

        gl.glGenVertexArrays(1, va, 0);
    }

    /**
     * Delete vertex array
     */
    public void Delete() {
        if (vbo != null) {
            for (int i = 0; i < maxVertexAttribs; i++) {
                if (isBOSet[i] == 1) {
                    gl.glDeleteBuffers(1, vbo, 0);
                    gl.glDisableVertexAttribArray(i);
                    vbo[i] = 0;
                    isBOSet[i] = 0;
                }
            }
            vbo = null;
        }

        if (va[0] > 0) {
            gl.glDeleteVertexArrays(1, va, 0);
            va[0] = 0;
        }
    }

    /**
     * Set array buffer with static_draw usage
     * 
     * @param idx
     *            Index of vertex buffer object
     * @param type
     *            Type of vertex buffer object (GL_BYTE, GL_SHORT,...)
     * @param dim
     *            Dimension of vertices (2, 3, 4)
     * @param data
     *            Vertex data
     * 
     * @return true if array buffer could be initialized.
     */
    public boolean SetArrayBuffer(int idx, int type, int dim, Buffer data) {
        return SetArrayBuffer(idx, type, dim, data, GL.GL_STATIC_DRAW);
    }
    
    /**
     * Set array buffer
     * 
     * @param idx
     *            Index of vertex buffer object
     * @param type
     *            Type of vertex buffer object (GL_BYTE, GL_SHORT,...)
     * @param dim
     *            Dimension of vertices (2, 3, 4)
     * @param data
     *            Vertex data
     * @param usage
     *            Usage hint (GL_STATIC_DRAW,...)
     * @return true if array buffer could be initialized.
     */
    public boolean SetArrayBuffer(int idx, int type, int dim, Buffer data,
            int usage) {
        if (idx >= maxVertexAttribs) {
            System.err.println(
                    "Error in VertexArray::CreateVBO()... index out of range!");
            return false;
        }

        if (isBOSet[idx] == 1) {
            gl.glDeleteBuffers(1, vbo, idx);
            vbo[idx] = 0;
        }

        this.vboType[idx] = type;
        this.vboUsage[idx] = usage;
        this.dim[idx] = dim;

        switch (type) {
            default :
            case GL3.GL_BYTE : {
                sizeOfData[idx] = 1;
                break;
            }
            case GL3.GL_SHORT : {
                sizeOfData[idx] = Short.BYTES;
                break;
            }
            case GL3.GL_INT : {
                sizeOfData[idx] = Integer.BYTES;
                break;
            }
            case GL3.GL_FLOAT : {
                sizeOfData[idx] = Float.BYTES;
                break;
            }
            case GL3.GL_DOUBLE : {
                sizeOfData[idx] = Double.BYTES;
                break;
            }
        }

        //FloatBuffer fb = (FloatBuffer)data;
        //System.err.println(fb.get(3) + " " + fb.get(4));

        gl.glGenBuffers(1, vbo, idx);

        Bind();
        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, vbo[idx]);
        gl.glEnableVertexAttribArray(idx);
        gl.glBufferData(GL3.GL_ARRAY_BUFFER,
                sizeOfData[idx] * numVertices * dim, data, usage);

        if (type == GL3.GL_BYTE || type == GL3.GL_SHORT || type == GL3.GL_INT) {
            gl.glVertexAttribIPointer(idx, dim, type, 0, 0);
        }
        else if (type == GL3.GL_DOUBLE) {
            gl.glVertexAttribLPointer(idx, dim, type, 0, 0);
        }
        else {
            gl.glVertexAttribPointer(idx, dim, type, false, 0, 0);
        }
        Release();

        isBOSet[idx] = 1;
        return true;
    }

    public void SetElementBuffer(int idx, int numElems, IntBuffer data) {
        // TODO
    }

    /**
     * Set sub array buffer
     * 
     * @param idx
     *            Index of vertex buffer object
     * @param offset
     *            Offset
     * @param num
     *            Number of vertices to be inserted
     * @param data
     *            Vertex data
     * @return true if vertices could be set
     */
    public boolean SetSubArrayBuffer(int idx, int offset, int num,
            Buffer data) {
        if (offset > numVertices || offset + num > numVertices
                || idx >= maxVertexAttribs) {
            System.err.println("VBO error: out of range!");
            return false;
        }
        if (isBOSet[idx] != 1) {
            System.err.println("VBO error: buffer not set!");
            return false;
        }

        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, vbo[idx]);
        gl.glBufferSubData(GL3.GL_ARRAY_BUFFER,
                offset * sizeOfData[idx] * dim[idx], num * sizeOfData[idx],
                data);
        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
        return true;
    }

    public boolean GetSubArrayBuffer(int idx, int offset, int num,
            Buffer data) {
        if (offset > numVertices || offset + num > numVertices
                || idx >= maxVertexAttribs) {
            return false;
        }
        if (isBOSet[idx] != 1) {
            return false;
        }

        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, vbo[idx]);
        gl.glGetBufferSubData(GL3.GL_ARRAY_BUFFER,
                offset * sizeOfData[idx] * dim[idx], num * sizeOfData[idx],
                data);
        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
        return true;
    }

    /**
     * Get maximum number of vertex attributes.
     * 
     * @return
     */
    public int GetMaxVertexAttribs() {
        return this.maxVertexAttribs;
    }

    /**
     * Get number of vertices in the vertex array.
     * 
     * @return
     */
    public int GetNumVertices() {
        return this.numVertices;
    }

    /**
     * Release vertex array object.
     */
    public void Release() {
        // gl.glBindVertexArray(0);
    }
}
