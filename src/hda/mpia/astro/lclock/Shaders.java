package hda.mpia.astro.lclock;

public class Shaders {
    
    static String fChartShaderText = 
        "#version 330\n" +
        "uniform sampler2D  tex;\n" +
        "in vec2 texCoords;\n" +
        "layout(location = 0) out vec4 fragColor;\n" +
        "void main() {\n" +
        "    vec2 tc = texCoords*1.05;\n" +
        "    float dist = length(tc);\n" +
        "    if (dist > 1.0) discard;\n" +
        "    fragColor = texture(tex, 0.5*tc + vec2(0.5));\n" +
        "}\n";
    
    
    static String vChartShaderText = 
            "#version 330\n" +
            "uniform mat4 projMX;\n" +
            "layout(location = 0) in vec2 position;\n" +
            "out vec2 texCoords;\n" +
            "void main() {\n" +
            "    gl_Position = projMX * vec4(position, 0, 1);\n" +
            "    texCoords = position;\n" +
            "}\n";

    static String fLineShaderText = 
            "#version 330\n" +
            "layout(location = 0) out vec4 fragColor;\n" +
            "void main() {\n" +
            "    fragColor = vec4(1,0,0,1);\n" +
            "}\n";
    
    static String vLineShaderText = 
            "#version 330\n" +
            "#define PI  3.1415926\n" +
            "uniform mat4 projMX;\n" +
            "uniform int hemisphere;\n" +
            "uniform float rot;\n" + 
            "layout(location = 0) in vec2 position;\n" +
            "\n" +
            "void main() {\n" +
            "    vec4 verts = vec4(position, 0, 1);\n" +
            "    mat2 rm = mat2(cos(rot), -sin(rot), sin(rot), cos(rot));\n" +
            "    verts.xy = rm * verts.xy;\n" +
            "    gl_Position = projMX * verts;\n" +
            "}\n";
    
    static String fMeridianShaderText = 
            "#version 330\n" +
            "layout(location = 0) out vec4 fragColor;\n" +
            "void main() {\n" +
            "    fragColor = vec4(0,0,0,1);\n" +
            "}\n";

    
    static String vMeridianShaderText = 
            "#version 330\n" +
            "uniform mat4 projMX;\n" +
            "uniform int hemisphere;\n" +
            "uniform float meridSize;\n" +
            "layout(location = 0) in vec2 position;\n" +
            "\n" +
            "void main() {\n" +
            "    vec4 verts = vec4(position, 0, 1);\n" +
            "    mat2 rm = mat2(-1,0,0,-1) * hemisphere + (1-hemisphere)*mat2(1,0,0,1);\n" +
            "    verts.xy = rm * verts.xy;\n" +
            "    verts.y += (hemisphere - 0.5)*1.8;\n" +
            "    gl_Position = projMX * verts;\n" +
            "}\n";

    
    static String fRingShaderText = 
            "#version 330\n" +
            "#define PI  3.1415926\n" +
            "uniform sampler2D tex;\n" +
            "uniform float rot;\n" +
            "uniform int hemisphere;\n" +
            "in vec2 texCoords;\n" +
            "layout(location = 0) out vec4 fragColor;\n" +
            "void main() {\n" +
            "    float dist = length(texCoords);\n" +
            "    if (dist < 0.90 || dist > 1.0) discard;\n" +
            "    float phi = atan(texCoords.y, texCoords.x) + rot;\n" +
            "    float angle = rot + hemisphere * PI;\n" +
            "    mat2 rm = mat2(cos(angle), sin(angle), -sin(angle), cos(angle));\n" +
            "    vec2 tc = rm * texCoords;\n" +
            "    vec4 color = texture(tex, 0.5*tc + 0.5);\n" +
            "    fragColor = vec4(vec3(0.9,0.9,0) * color.rgb, 1);\n" +
            "}\n";

    public static String vRingShaderText = 
            "#version 330\n" +
            "uniform mat4 projMX;\n" +
            "layout(location = 0) in vec2 position;\n" +
            "out vec2 texCoords;\n" +
            "void main() {\n" +
            "    gl_Position = projMX * vec4(position, 0, 1);\n" +
            "    texCoords = position;\n" +
            "}\n";

}
