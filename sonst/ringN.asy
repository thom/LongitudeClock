//
//

usepackage("gensymb");


settings.render=12;
settings.outformat = "png";
settings.prc = false;


defaultpen(fontsize(14pt)+linewidth(1));
import geometry;
import graph;

size(16cm,0);

pair ll = (-70.0,-70.0);
pair ur = ( 70.0, 70.0);

real r  = 10.0;
real dri = 0.35;
real dro = 0.6;
real dsb = 0.25;
real dsm = 0.15;

filldraw(scale(r+dro) * unitcircle, white);

draw(scale(r-dri) * unitcircle, 0.5bp+black);
draw(scale(r+dro) * unitcircle, 0.5bp+black);

for(int h=0; h < 24; h += 1) {
    real p = h / 24.0 * 360.0;
    real alpha = radians(p + 90);
    string t = format("%d", h);
    label(rotate(p-180)*t, r*(cos(alpha), sin(alpha)), black);
}

for(real a=0.0; a < 360.0; a+=15.0/6.0) {
    real alpha = radians(a);    
    draw((r+dro)*(cos(alpha), sin(alpha)) -- (r+dro-dsm)*(cos(alpha), sin(alpha)), 0.5bp+black);
}

for(real a=0.0; a < 360.0; a+=15.0) {
    real alpha = radians(a);    
    draw((r+dro)*(cos(alpha), sin(alpha)) -- (r+dro-dsb)*(cos(alpha), sin(alpha)), 1bp+black);
}


